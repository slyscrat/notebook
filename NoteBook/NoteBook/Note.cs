﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoteBook
{    
    class Note
    {
        private static int idCounter = 0;
        private static int IdCounter { get; set; }
        private static List<int> RemovedId = new List<int>();
        public static void AddToRemovedId(int num)
        {
            RemovedId.Add(num);
        }
        public int Id { get; set; }
        public string SurName { get; set; }
        public string FirstName { get; set; }
        public string Patronymic { get; set; }
        public string PhoneNumber { get; set; }
        public string Country { get; set; }
        public DateTime BirthDate { get; set; }
        public string Organisation { get; set; }
        public string JobPosition { get; set; }
        public string OtherInfo { get; set; }
        public Note()
        {
            if (RemovedId.Count == 0)
            {
                this.Id = ++IdCounter;
            }
            else
            {
                this.Id = RemovedId.ElementAt(0);
                RemovedId.RemoveAt(0);
            }
        }

        public override string ToString()
        {
            string note = "  Surname: " + this.SurName + "\n" 
                            + "     Firstname: " + this.FirstName + "\n" 
                            + "     Phone: " + this.PhoneNumber;
            return note;
        }
        public string FullInfo()
        {
            string note = "Surname: " + this.SurName + "\n" 
                            + "Firstname: " + this.FirstName + "\n"
                            + "Patronymic: " + this.Patronymic + "\n" 
                            + "Phone: " + this.PhoneNumber + "\n" 
                            + "Country: " + this.Country + "\n" 
                            + "Birth Date: " + this.BirthDate.ToShortDateString() + "\n" 
                            + "Organisation: " + this.Organisation + "\n" 
                            + "Job position: " + this.JobPosition + "\n" 
                            + "Other: " + this.OtherInfo;
            return note;
        }

    }
}
