﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Text.RegularExpressions;

namespace NoteBook
{
    class Notebook
    {
        static Dictionary<int, Note> dictionary = new Dictionary<int, Note>();
        static void Main(string[] args)
        {
            EnterMenu();
        }
        static void EnterMenu()
        {
            Board("Welcome to my NoteBook application (version 0.1)!");
            ;
            bool go = true;
            while (go)
            {
                Board("Main Menu");
                Console.WriteLine("\nWhat shall we do now?\n");
                Console.WriteLine("1 - Add new person to NoteBook");
                Console.WriteLine("2 - Edit existing person in NoteBook");
                Console.WriteLine("3 - Delete existing person in NoteBook");
                Console.WriteLine("4 - Look at existing person in NoteBook");
                Console.WriteLine("5 - Short info about all existing persons in NoteBook");
                Console.WriteLine("6 - Exit application");

                switch (IntEnter(6))
                {
                    case 1:
                        Console.Clear();
                        Note note = new Note();
                        Board("Adding new note");
                        Create(note);
                        dictionary.Add(note.Id, note);
                        break;
                    case 2:
                        if (!IsEmpty())
                            Edit();
                        break;
                    case 3:
                        if (!IsEmpty())
                            Delete();
                        break;
                    case 4:
                        if (!IsEmpty())
                            LookAt();
                        break;
                    case 5:
                        Console.Clear();
                        Board("Looking notes. Press any key to back to previous menu");
                        Look();
                        Console.ReadKey();
                        break;
                    case 6:
                        Leave(ref go);
                        break;
                }
                Console.Clear();
            }
            Console.WriteLine("Bye! Good luck!");
            Thread.Sleep(2500);
        }
        static Note Create(Note note)
        {
            Console.WriteLine("$ attribute - mean that field is NECESSARY to enter!");
            Console.WriteLine("All NOT necessary fields can be skipped by pressig <Enter>!\n");

            Console.WriteLine("Enter surname ($) [only letters and '-' symbol, first letter must be in upper case]: ");
            Console.Write(note.SurName + " -> ");
            note.SurName = Enter(true, true);

            Console.WriteLine("Enter firstname ($) [only letters and '-' symbol, first letter must be in upper case]: ");
            Console.Write(note.FirstName + " -> ");
            note.FirstName = Enter(true, true);

            Console.WriteLine("Enter patronymic [only letters and '-' symbol, first letter must be in upper case]: ");
            Console.Write(note.Patronymic + " -> ");
            note.Patronymic = Enter(false, true);

            Console.WriteLine("Enter phone ($) [from 6 to 14 numbers]: ");
            Console.Write(note.PhoneNumber + " -> ");
            note.PhoneNumber = Enter(true, false);

            Console.WriteLine("Enter country ($) [only letters and '-' symbol, first letter must be in upper case]: ");
            Console.Write(note.Country + " -> ");
            note.Country = Enter(true, true);

            Console.WriteLine("Enter birth date [format dd.mm.yy]: ");
            if (note.BirthDate == DateTime.MinValue) Console.Write(" -> ");
            else Console.Write(note.BirthDate.ToShortDateString() + " -> ");
            note.BirthDate = DataEnter();

            Console.WriteLine("Enter organisation: ");
            Console.Write(note.Organisation + " -> ");
            note.Organisation = Console.ReadLine();

            Console.WriteLine("Enter job position: ");
            Console.Write(note.JobPosition + " -> ");
            note.JobPosition = Console.ReadLine();

            Console.WriteLine("Enter other info: ");
            Console.Write(note.OtherInfo + " -> ");
            note.OtherInfo = Console.ReadLine();

            Console.WriteLine("Your information sucessfully saved! Press any button to return to the main menu");
            Console.ReadKey();
            return note;

        }

        static void Edit()
        {
            Board("Editing note");
            Console.WriteLine("Enter number of note to edit");
            Console.WriteLine("Enter 0 to back to previous menu");
            Look();
            int num = IdEnter();
            if (num == 0) return;
            Create(dictionary[num]);
            Console.Clear();
            Board("Changes saved! Press any key to back to previous menu");
            Look();
        }
        static void Delete()
        {
            Console.Clear();
            Board("Deleting note");
            Console.WriteLine("Enter number of note to delete");
            Console.WriteLine("Enter 0 to back to previous menu");
            Look();
            int key = IdEnter();
            if (key == 0) return;
            dictionary.Remove(key);
            Note.AddToRemovedId(key);
            Console.Clear();
            Board("Deleted sucessfully! Press any key to back to previous menu");
            Look();
            Console.ReadKey();
        }
        static bool IsEmpty()
        {
            Console.Clear();
            if (dictionary.Count == 0)
            {
                Console.WriteLine("Your notebook is empty right now! Come back later. Press any key to back to previous menu");
                Console.ReadLine();
                return true;
            }
            return false;
        }
        static void LookAt()
        {
            Board("Full info");
            Console.WriteLine("Enter number of note to look full info");
            Console.WriteLine("Enter 0 to back to previous menu");
            Look();
            int num = IdEnter();
            if (num == 0) return;
            Console.Clear();
            Console.WriteLine(dictionary[num].FullInfo());
            Console.WriteLine("\nPress any key to back to main menu");
            Console.ReadKey();
        }
        static void Look()
        {
            Console.WriteLine("----------------------------------");
            foreach (var item in dictionary)
            {
                Console.WriteLine("{0}) {1}", item.Key, item.Value);
                Console.WriteLine("----------------------------------");
            }
        }
        static void Leave(ref bool go)
        {
            Console.Clear();
            Board("Exiting");
            Console.WriteLine("Are you sure you want to exit? All the information will be deleted!");
            Console.WriteLine("1 - Yes, I want to go\n[Anything else] - No, I want to continue use this great app! Back to previous menu.");
            if (Console.ReadLine().Equals("1")) go = false;
        }
        static int IdEnter()
        {
            int num;
            while (true)
            {
                try
                {
                    num = int.Parse(Console.ReadLine());
                    if (!dictionary.ContainsKey(num) && num != 0)
                    {
                        Console.WriteLine("Input value is not matching with id list saved in Notebook");
                        continue;
                    }
                    break;
                }
                catch (Exception)
                {
                    Console.WriteLine("Oh no, I can't understand your input. Please, enter valid integer value!");
                }
            }
            return num;
        }
        static int IntEnter(int upperValue)
        {
            int num;
            while (true)
            {
                try
                {
                    num = int.Parse(Console.ReadLine());
                    if (num > upperValue)
                    {
                            Console.WriteLine("Input value must be not greater than {0}", upperValue);
                            continue;
                    }
                    break;
                }
                catch (Exception)
                {
                    Console.WriteLine("Oh no, I can't understand your input. Please, enter valid integer value!");
                }
            }
            return num;
        }
        static DateTime DataEnter()
        {
            DateTime date = DateTime.Now;
            while (true)
            {
                try
                {
                    string input = Console.ReadLine();
                    if (string.IsNullOrWhiteSpace(input))
                    {
                        return DateTime.MinValue;
                    }
                    date = DateTime.Parse(input);
                    if (date < DateTime.Parse("12/12/1900") || date > DateTime.Parse("12/12/2017"))
                    {
                        Console.WriteLine("You might be kidden, this date can't be your birthday =D\nTry again!");
                        continue;
                    }
                    break;
                }
                catch(Exception)
                {
                    Console.WriteLine("Something went wrong with your input date. Try again!");
                }
            }
            return date;
        }
        static string Enter(bool isNecessary, bool isName)
        {
            string input = "";
            Regex reg;
            if (isName) reg = new Regex("^[A-ZА-ЯЁ][a-zа-яё]*(?:-[A-ZА-ЯЁ][a-zа-яё]*)?\r?$");
            else reg = new Regex("^(\\s*)?(\\+)?([- _():=+]?\\d[- _():=+]?){6,14}(\\s*)?$");        // 11 цифр в ру стандарте

            while (true)
            {
                try
                {
                    input = Console.ReadLine();
                    if (reg.Match(input).Success)
                    {
                        break;
                    }
                    else if (string.IsNullOrWhiteSpace(input) && !isNecessary)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Format of entered data incorrect! Try again!");
                    }
                }
                catch (FormatException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return input;
        }
        static void Board(string title)
        {
            for (int i = 0; i < title.Length; i++)
            {
                Console.Write("*");
            }
            Console.Write("\n" + title + "\n");
            for (int i = 0; i < title.Length; i++)
            {
                Console.Write("*");
            }
            Console.WriteLine();
        }
    }
}
